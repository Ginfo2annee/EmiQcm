/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Hajar MDD
 */

    
import java.sql.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
public class VerifieReponse {
    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    
    static Connection connection = null; 
    static Statement statement = null;
    static ResultSet resultSet ;
    static ResultSet resultSet2 ;
    static ResultSetMetaData metaData;
    
public static void init(){
 try{         
             Class.forName("com.mysql.jdbc.Driver");
             connection= DriverManager.getConnection("jdbc:mysql://localhost:3306/quiz-it-up?zeroDateTimeBehavior=convertToNull","root","root");
        }catch(Exception e){
             e.printStackTrace();
        }
}
public static ResultSet VerifieReponseQuestion(String Reponse,String Question) throws SQLException{
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement("select * from Reponse,Question where Reponse=? and Enonce=? and Reponse.IDq=Question.IDQuestion");
            ps.setString(1, Reponse);
            ps.setString(2, Question);
            
        } catch (SQLException ex) {
            Logger.getLogger(ConnectBD.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ps.executeQuery();
}
} 
