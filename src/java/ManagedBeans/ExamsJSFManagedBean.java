/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ManagedBeans;

import entities.Exam;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import modele.ExamFacade;


@Named(value = "examsJSFManagedBean")
@SessionScoped
public class ExamsJSFManagedBean implements Serializable {

    @EJB
    private ExamFacade examFacade;
    
    /**
     * Creates a new instance of ExamsJSFManagedBean
     */
    public ExamsJSFManagedBean() {
    }
    
    public List<Exam> findExams(){
        return examFacade.findAll();
    }
}
