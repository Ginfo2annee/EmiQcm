/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ManagedBeans;

import entities.Question;
import entities.Reponse;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import modele.QuestionFacade;
import modele.ReponseFacade;

@Named(value = "questionJSFManagedBean")
@SessionScoped
public class QuestionJSFManagedBean implements Serializable {

    @EJB
    private ReponseFacade reponseFacade;

    @EJB
    private QuestionFacade questionFacade;
    
    private int idExam;
    private List<Question> questions;
    private List<List<Reponse>> reponses;
    private int idDesQuestions = -1;
    private int notedeBareme = 0;
    private int note = 0;
    private String[][] selectedRep ;

    public int getnote() {
        return note;
    }
     public QuestionJSFManagedBean() {
    }Z
    public void setnote(int note) {
        this.note = note;
    }

    public String affichernote(){
        
        for(int i=0; i<selectedRep.length; i++)
            note += selectedRep[i].length;
        return "notejsf?note=" + note;
    }
    
    public String[][] getSelectedRep() {
        return selectedRep;
    }

    public void setSelectedRep(String[][]selectedRep) {
        this.selectedRep = selectedRep;
    }
    
    
    public int getnotedeBareme() {
        return notedeBareme;
    }

    public void setnotedeBareme(int note) {
        this.notedeBareme = note;
    }
    
    
    public int getidDesQuestions() {
        return idDesQuestions;
    }

    public void setidDesQuestions(int idDesQuestions) {
        this.idDesQuestions = idDesQuestions;
    }
    
    
    public int getIdExam() {
        return idExam;
    }

    public void setIdExam(int idExam) {
        this.idExam = idExam;
    }
    
    public List<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }
     public String[] getSelReponses() {
         idDesQuestions = (idDesQuestions + 1)%questions.size();
        return selectedRep[idDesQuestions];
    }
     
    public List<Reponse> getReponses() {
       idDesQuestions = (idDesQuestions + 1)%questions.size();
        return reponses.get(idDesQuestions);
    }

    public void setReponses(List<List<Reponse>> reponses) {
        this.reponses = reponses;
        
    }
    
    public void findQuestions(){
        idDesQuestions = -1;
        notedeBareme = 0;
        note = 0;
        questions = questionFacade.findByEx(idExam);
        selectedRep = new String[questions.size()][4];
    }
    
    public void findReponses(){
        reponses = reponseFacade.findByQuestions(questions);
        for(int i=0; i< reponses.size(); i++){
            for(int j=0; j<reponses.get(i).size(); j++){
                notedeBareme += reponses.get(i).get(j).getIsGood();
            }
        }
    }
    
    public String PasserExam(int idExam){
        return "Examjsf?idExam=" + idExam;
    }
    
   
    
}
