/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;


@Entity
@Table(name = "chapitre")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Chapitre.findAll", query = "SELECT c FROM Chapitre c")
    , @NamedQuery(name = "Chapitre.findByIdchapitre", query = "SELECT c FROM Chapitre c WHERE c.idchapitre = :idchapitre")})
public class Chapitre implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "idchapitre")
    private Integer idchapitre;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idChapitre")
    private List<Question> questionList;
    @JoinColumn(name = "idCours", referencedColumnName = "idCours")
    @ManyToOne(optional = false)
    private Cours idCours;

    public Chapitre() {
    }

    public Chapitre(Integer idchapitre) {
        this.idchapitre = idchapitre;
    }

    public Integer getIdchapitre() {
        return idchapitre;
    }

    public void setIdchapitre(Integer idchapitre) {
        this.idchapitre = idchapitre;
    }

    @XmlTransient
    public List<Question> getQuestionList() {
        return questionList;
    }

    public void setQuestionList(List<Question> questionList) {
        this.questionList = questionList;
    }

    public Cours getIdCours() {
        return idCours;
    }

    public void setIdCours(Cours idCours) {
        this.idCours = idCours;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idchapitre != null ? idchapitre.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Chapitre)) {
            return false;
        }
        Chapitre other = (Chapitre) object;
        if ((this.idchapitre == null && other.idchapitre != null) || (this.idchapitre != null && !this.idchapitre.equals(other.idchapitre))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Chapitre[ idchapitre=" + idchapitre + " ]";
    }
    
}
