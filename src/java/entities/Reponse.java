/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;


@Entity
@Table(name = "reponse")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Reponse.findAll", query = "SELECT r FROM Reponse r")
    , @NamedQuery(name = "Reponse.findByIdReponse", query = "SELECT r FROM Reponse r WHERE r.idReponse = :idReponse")
    , @NamedQuery(name = "Reponse.findByReponse", query = "SELECT r FROM Reponse r WHERE r.reponse = :reponse")
    , @NamedQuery(name = "Reponse.findByIsGood", query = "SELECT r FROM Reponse r WHERE r.isGood = :isGood")
    , @NamedQuery(name = "Reponse.findByQuestion", query = "SELECT r FROM Reponse r WHERE r.idQestion = :idQestion")})
public class Reponse implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "idReponse")
    private Integer idReponse;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "reponse")
    private String reponse;
    @Basic(optional = false)
    @NotNull
    @Column(name = "isGood")
    private short isGood;
    @JoinColumn(name = "idQestion", referencedColumnName = "idQuestion")
    @ManyToOne(optional = false)
    private Question idQestion;

    public Reponse() {
    }

    public Reponse(Integer idReponse) {
        this.idReponse = idReponse;
    }

    public Reponse(Integer idReponse, String reponse, short isGood) {
        this.idReponse = idReponse;
        this.reponse = reponse;
        this.isGood = isGood;
    }

    public Integer getIdReponse() {
        return idReponse;
    }

    public void setIdReponse(Integer idReponse) {
        this.idReponse = idReponse;
    }

    public String getReponse() {
        return reponse;
    }

    public void setReponse(String reponse) {
        this.reponse = reponse;
    }

    public short getIsGood() {
        return isGood;
    }

    public void setIsGood(short isGood) {
        this.isGood = isGood;
    }

    public Question getIdQestion() {
        return idQestion;
    }

    public void setIdQestion(Question idQestion) {
        this.idQestion = idQestion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idReponse != null ? idReponse.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Reponse)) {
            return false;
        }
        Reponse other = (Reponse) object;
        if ((this.idReponse == null && other.idReponse != null) || (this.idReponse != null && !this.idReponse.equals(other.idReponse))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Reponse[ idReponse=" + idReponse + " ]";
    }
    
}
