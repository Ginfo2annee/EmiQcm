/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name = "qcm")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Qcm.findAll", query = "SELECT q FROM Qcm q")
    , @NamedQuery(name = "Qcm.findByIdQCM", query = "SELECT q FROM Qcm q WHERE q.idQCM = :idQCM")
    , @NamedQuery(name = "Qcm.findByConsigne", query = "SELECT q FROM Qcm q WHERE q.consigne = :consigne")})
public class Qcm implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "idQCM")
    private Integer idQCM;
    @Size(max = 50)
    @Column(name = "Consigne")
    private String consigne;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "qcm")
    private Exam exam;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idQCM")
    private List<Question> questionList;

    public Qcm() {
    }

    public Qcm(Integer idQCM) {
        this.idQCM = idQCM;
    }

    public Integer getIdQCM() {
        return idQCM;
    }

    public void setIdQCM(Integer idQCM) {
        this.idQCM = idQCM;
    }

    public String getConsigne() {
        return consigne;
    }

    public void setConsigne(String consigne) {
        this.consigne = consigne;
    }

    public Exam getExam() {
        return exam;
    }

    public void setExam(Exam exam) {
        this.exam = exam;
    }

    @XmlTransient
    public List<Question> getQuestionList() {
        return questionList;
    }

    public void setQuestionList(List<Question> questionList) {
        this.questionList = questionList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idQCM != null ? idQCM.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Qcm)) {
            return false;
        }
        Qcm other = (Qcm) object;
        if ((this.idQCM == null && other.idQCM != null) || (this.idQCM != null && !this.idQCM.equals(other.idQCM))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Qcm[ idQCM=" + idQCM + " ]";
    }
    
}
