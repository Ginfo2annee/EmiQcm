/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;


@Entity
@Table(name = "exam")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Exam.findAll", query = "SELECT e FROM Exam e")
    , @NamedQuery(name = "Exam.findByIdexam", query = "SELECT e FROM Exam e WHERE e.idexam = :idexam")
    , @NamedQuery(name = "Exam.findByCreneau", query = "SELECT e FROM Exam e WHERE e.creneau = :creneau")})
public class Exam implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "idexam")
    private Integer idexam;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "creneau")
    private String creneau;
    @JoinColumn(name = "idCours", referencedColumnName = "idCours")
    @ManyToOne(optional = false)
    private Cours idCours;
    @JoinColumn(name = "idgroupe", referencedColumnName = "idGroupe")
    @ManyToOne(optional = false)
    private Groupe idgroupe;
    @JoinColumn(name = "idexam", referencedColumnName = "idQCM", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Qcm qcm;

    public Exam() {
    }

    public Exam(Integer idexam) {
        this.idexam = idexam;
    }

    public Exam(Integer idexam, String creneau) {
        this.idexam = idexam;
        this.creneau = creneau;
    }

    public Integer getIdexam() {
        return idexam;
    }

    public void setIdexam(Integer idexam) {
        this.idexam = idexam;
    }

    public String getCreneau() {
        return creneau;
    }

    public void setCreneau(String creneau) {
        this.creneau = creneau;
    }

    public Cours getIdCours() {
        return idCours;
    }

    public void setIdCours(Cours idCours) {
        this.idCours = idCours;
    }

    public Groupe getIdgroupe() {
        return idgroupe;
    }

    public void setIdgroupe(Groupe idgroupe) {
        this.idgroupe = idgroupe;
    }

    public Qcm getQcm() {
        return qcm;
    }

    public void setQcm(Qcm qcm) {
        this.qcm = qcm;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idexam != null ? idexam.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Exam)) {
            return false;
        }
        Exam other = (Exam) object;
        if ((this.idexam == null && other.idexam != null) || (this.idexam != null && !this.idexam.equals(other.idexam))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Exam[ idexam=" + idexam + " ]";
    }
    
}
