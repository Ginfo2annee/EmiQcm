/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele;

import entities.Question;
import entities.Reponse;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless
public class ReponseFacade extends AbstractFacade<Reponse> {

    @PersistenceContext(unitName = "QuizzPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ReponseFacade() {
        super(Reponse.class);
    }

    public List<List<Reponse>> findByQuestions(List<Question> questions) {
        Question question;
        Query query = em.createNamedQuery("Reponse.findByQuestion");
        List<List<Reponse>> reponses = new ArrayList<List<Reponse>>();
        for(int i=0 ; i < questions.size(); i++){
            question = questions.get(i);
            query.setParameter("idQestion", question);
            reponses.add(new ArrayList((List<Reponse>)query.getResultList()));
        }
        return reponses;
    }
    
}
