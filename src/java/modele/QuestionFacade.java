/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele;

import entities.Qcm;
import entities.Question;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;


@Stateless
public class QuestionFacade extends AbstractFacade<Question> {

    @PersistenceContext(unitName = "QuizzPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public QuestionFacade() {
        super(Question.class);
    }

    public List<Question> findByEx(int idExam) {
        Query query = em.createNamedQuery("Qcm.findByIdQCM");
        query.setParameter("idQCM", idExam);
        Qcm qcm = (Qcm) query.getSingleResult();
        Query queryq = em.createNamedQuery("Question.findByQcm");
        queryq.setParameter("idQCM", qcm);
        List<Question> questions = (List<Question>) queryq.getResultList();
        return questions;
    }
    
}
