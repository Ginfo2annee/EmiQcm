/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele;

import entities.Chapitre;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


@Stateless
public class ChapitreFacade extends AbstractFacade<Chapitre> {

    @PersistenceContext(unitName = "QuizzPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ChapitreFacade() {
        super(Chapitre.class);
    }
    
}
