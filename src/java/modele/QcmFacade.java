/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele;

import entities.Qcm;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


@Stateless
public class QcmFacade extends AbstractFacade<Qcm> {

    @PersistenceContext(unitName = "QuizzPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public QcmFacade() {
        super(Qcm.class);
    }
    
}
