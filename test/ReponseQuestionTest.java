

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.sql.ResultSet;
import java.sql.SQLException;
import model.VerifieReponse;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Ahmed Yassin
 */
public class ReponseQuestionTest {
    
    
    
    
    
    @Test
    public void test() throws SQLException{
        VerifieReponse.init();
        ResultSet rs=VerifieReponse.VerifieReponseQuestion("jee","Q1");
        String output=null;
        while(rs.next())
            output=rs.getString("Reponse.reponse");
        assertEquals("jee",output);
    }

    
   }

    

