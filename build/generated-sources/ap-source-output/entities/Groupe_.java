package entities;

import entities.Exam;
import entities.User;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-06-23T15:05:32")
@StaticMetamodel(Groupe.class)
public class Groupe_ { 

    public static volatile ListAttribute<Groupe, Exam> examList;
    public static volatile SingularAttribute<Groupe, User> idApprenant;
    public static volatile SingularAttribute<Groupe, Integer> idGroupe;

}