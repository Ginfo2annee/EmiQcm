package entities;

import entities.Exam;
import entities.Question;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-06-23T15:05:32")
@StaticMetamodel(Qcm.class)
public class Qcm_ { 

    public static volatile SingularAttribute<Qcm, Exam> exam;
    public static volatile SingularAttribute<Qcm, String> consigne;
    public static volatile SingularAttribute<Qcm, Integer> idQCM;
    public static volatile ListAttribute<Qcm, Question> questionList;

}