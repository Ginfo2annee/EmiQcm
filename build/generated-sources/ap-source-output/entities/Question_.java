package entities;

import entities.Chapitre;
import entities.Qcm;
import entities.Reponse;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-06-23T15:05:32")
@StaticMetamodel(Question.class)
public class Question_ { 

    public static volatile SingularAttribute<Question, String> question;
    public static volatile SingularAttribute<Question, String> consigne;
    public static volatile SingularAttribute<Question, Integer> idQuestion;
    public static volatile SingularAttribute<Question, Qcm> idQCM;
    public static volatile ListAttribute<Question, Reponse> reponseList;
    public static volatile SingularAttribute<Question, Chapitre> idChapitre;

}