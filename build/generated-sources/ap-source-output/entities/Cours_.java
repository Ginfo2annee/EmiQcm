package entities;

import entities.Chapitre;
import entities.Exam;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-06-23T15:05:32")
@StaticMetamodel(Cours.class)
public class Cours_ { 

    public static volatile ListAttribute<Cours, Exam> examList;
    public static volatile SingularAttribute<Cours, Integer> idCours;
    public static volatile ListAttribute<Cours, Chapitre> chapitreList;
    public static volatile SingularAttribute<Cours, String> nom;

}