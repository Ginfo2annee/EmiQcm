package entities;

import entities.Question;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-06-23T15:05:32")
@StaticMetamodel(Reponse.class)
public class Reponse_ { 

    public static volatile SingularAttribute<Reponse, String> reponse;
    public static volatile SingularAttribute<Reponse, Short> isGood;
    public static volatile SingularAttribute<Reponse, Question> idQestion;
    public static volatile SingularAttribute<Reponse, Integer> idReponse;

}