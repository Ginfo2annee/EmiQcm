package entities;

import entities.Cours;
import entities.Question;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-06-23T15:05:32")
@StaticMetamodel(Chapitre.class)
public class Chapitre_ { 

    public static volatile SingularAttribute<Chapitre, Integer> idchapitre;
    public static volatile SingularAttribute<Chapitre, Cours> idCours;
    public static volatile ListAttribute<Chapitre, Question> questionList;

}