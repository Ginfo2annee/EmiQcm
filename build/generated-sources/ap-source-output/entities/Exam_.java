package entities;

import entities.Cours;
import entities.Groupe;
import entities.Qcm;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-06-23T15:05:32")
@StaticMetamodel(Exam.class)
public class Exam_ { 

    public static volatile SingularAttribute<Exam, String> creneau;
    public static volatile SingularAttribute<Exam, Integer> idexam;
    public static volatile SingularAttribute<Exam, Cours> idCours;
    public static volatile SingularAttribute<Exam, Groupe> idgroupe;
    public static volatile SingularAttribute<Exam, Qcm> qcm;

}